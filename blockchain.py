from datetime import datetime
import hashlib
import json 



class Block:
    payload = None
    blockHeight = 0
    timestamp = None
    previousHash = None
    currentHash = None

    def __init__(self, payload, blockHeight, timestamp, previousHash):
        self.payload = payload
        self.blockHeight = blockHeight
        self.timestamp = timestamp
        self.previousHash = previousHash

        jsonBlock = {}
        jsonBlock['payload'] = self.payload
        jsonBlock['blockHeight'] = self.blockHeight
        jsonBlock['timestamp'] = self.timestamp
        jsonBlock['previousHash'] = self.previousHash

        self.jsonBlock = json.dumps(jsonBlock)
        self.jsonBlockByte = bytearray()
        self.jsonBlockByte.extend(self.jsonBlock.encode())

        sha256 = hashlib.sha256()
        sha256.update(self.jsonBlockByte)
        self.currentHash = sha256.hexdigest()
        print(jsonBlock,"\n",self.currentHash)


class Blockchain:
    blockchain = []
    def __init__(self):
        self.blockchain.append(Block("GENESIS BLOCK", 0, datetime.timestamp(datetime.now()) % 10, 0))

    def generateNextBlock(self,payload):
        previousBlock = self.getLatestBlock()
        blockHeight = previousBlock.blockHeight + 1
        timestamp = datetime.timestamp(datetime.now()) # % 10
        previousHash = previousBlock.currentHash
        newBlock = Block(payload, blockHeight, timestamp, previousHash)
        self.blockchain.append(newBlock)


    def getLatestBlock(self):
        return self.blockchain[-1]

    def validateBlockchain(self):
        validFlag = True
        for i in range(1,len(self.blockchain)):
            print(i)
            validFlag = self.validateBlock(self.blockchain[i], self.blockchain[i-1])
            if(not validFlag):
                print("Blockchain invalid at blockheight %d"%(i))
                break
        if not validFlag:
            print("Blockchain invalid")
        else:
            print("Blockchain valid")
            
    def validateBlock(self, block, previousBlock):
        validFlag = True
        if block.blockHeight != previousBlock.blockHeight+1 :
            validFlag = False
            print("Incorrect block height")
        if block.previousHash != previousBlock.currentHash:
            validFlag = False
            print("Incorrect previous Hash")

        # Check if hash is correct
        jsonBlock = {}
        jsonBlock['payload'] = block.payload
        jsonBlock['blockHeight'] = block.blockHeight
        jsonBlock['timestamp'] = block.timestamp
        jsonBlock['previousHash'] = block.previousHash
        jsonBlock = json.dumps(jsonBlock)
        sha256 = hashlib.sha256()
        sha256.update(jsonBlock.encode())
        if(block.currentHash != sha256.hexdigest()):
            validFlag = False
            print("Incorrect Hash")

        return validFlag

blockchain = Blockchain()
blockchain.generateNextBlock("First block")
blockchain.validateBlockchain()
